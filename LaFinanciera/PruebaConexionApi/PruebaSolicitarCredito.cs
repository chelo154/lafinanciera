﻿using System;
using LaFinanciera.Datos;
using LaFinanciera.Datos.ConectorAPI;
using LaFinanciera.Dominio;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaConexionApi
{
    [TestClass]
    public class PruebaSolicitarCredito
    {
        public Plan _plan;
        [TestInitialize]
        public void PrepararPrueba()
        {
            Financiera.LaFinanciera.Reglas.PorcentajeInteresMensual = 10;
            RepositorioLocal.crearPlanes();
            RepositorioLocal.IniciarCliente();
            
        }
        [TestMethod]
        public void BuscarClienteRegistradoPorDni()
        {
            //Configuracion
            int dni = 33000001;
            //Ejecucion
            var cliente = RepositorioLocal.BuscarCliente(dni);
            //Validacion
            Assert.IsNotNull(cliente);
        }
        [TestMethod]
        public void BuscarClienteRegistradoPorNombre()
        {
            //Configuracion
            string apellido = "Taddei";
            string nombre = "Ivan";
            //Ejecucion
            var cliente = RepositorioLocal.BuscarCliente(nombre, apellido);
            //Validacion
            Assert.IsNotNull(cliente);
        }
        [TestMethod]
        public void BuscarClienteNoRegistadoPorDni()
        {
            //Configuracion
            int dni = 12;
            //Ejecucion
            var cliente = RepositorioLocal.BuscarCliente(dni);
            //Validacion
            Assert.IsNull(cliente);
        }
        [TestMethod]
        public void BuscarClienteNoRegistadoPorNombre()
        {
            //Configuracion
            string nombre = "ASKJLASJ";
            string apellido = "Ajkslajs";
            //Ejecucion
            var cliente = RepositorioLocal.BuscarCliente(nombre, apellido);
            //Validacion
            Assert.IsNull(cliente);
        }
        [TestMethod]
        public void buscarClienteRegistradoConDeudas()
        {
            //Configuracion
            int dni = 33000001;
            int cantidadCredActivos = 0;
            var cliente = RepositorioLocal.BuscarCliente(dni);
            var conexion = AdministradorConexiones.Administrador.ObtenerConectorServicioPublico();
            conexion.Conectar();
            //Ejecucion
            var deudas = conexion.ValidarEstadoCliente(GrupoFinanciera.Grupo.GroupID, cliente.Dni, ref cantidadCredActivos);
            //Validacion
            Assert.IsTrue(deudas);
        }
        [TestMethod]
        public void buscarClienteRegistradoSinDeudas()
        {
            //Configuracion
            int dni = 33000100;
            int cantidadCredActivos = 0;
            var cliente = RepositorioLocal.BuscarCliente(dni);
            var conexion = AdministradorConexiones.Administrador.ObtenerConectorServicioPublico();
            conexion.Conectar();
            //Ejecucion
            var deudas = conexion.ValidarEstadoCliente(GrupoFinanciera.Grupo.GroupID,cliente.Dni, ref cantidadCredActivos);
            //Validacion
            Assert.IsFalse(deudas);
        }
        [TestMethod]
        public void GenerarCreditoPlanCuotaAdelantada()
        {
            //COnfiguracion
            double esperado = 77000; 
            Credito c = new Credito();
            //Ejecucion
            var obtenido = c.seleccionarModoPago(100000, RepositorioLocal.BuscarPlanes(1));
            //Validacion
            Assert.AreEqual(esperado, obtenido);
        }
        [TestMethod]
        public void GenerarCreditoPlanCuotaVencida()
        {
            //Configuracion
            double esperado = 107800;
            Credito c = new Credito();
            //Ejecucion
            var obtenido = c.seleccionarModoPago(100000, RepositorioLocal.BuscarPlanes(3));
            //Validacion
            Assert.AreEqual(esperado, obtenido);
        }
    }
}
