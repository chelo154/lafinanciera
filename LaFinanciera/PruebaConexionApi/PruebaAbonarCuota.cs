﻿using System;
using LaFinanciera.Datos;
using LaFinanciera.Dominio;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaConexionApi
{
    [TestClass]
    public class PruebaAbonarCuota
    {
        private Cliente _cliente;
        
        
        [TestInitialize]
        public void iniciarTest()
        {
            RepositorioLocal.IniciarCliente();
            RepositorioLocal.crearPlanes();
            _cliente = RepositorioLocal.BuscarCliente(33000100);           
            Plan b = RepositorioLocal.BuscarPlanes(3);
            Credito b1 = new Credito();
            b1.seleccionarModoPago(100000, b);
            _cliente.Creditos.Add(b1);
        }
        [TestMethod]
        public void ObtenerCuotasImpagas()
        {
            //Configuracion
            Credito a1 = new Credito();
            a1.seleccionarModoPago(100000, RepositorioLocal.BuscarPlanes(1));
            _cliente.Creditos.Add(a1);
            //Ejecucion
            var cuotasimpagas = _cliente.ObtenerCuotasImpagas().Count;
            //Validacion
            Assert.AreEqual(5, cuotasimpagas);
        }
        [TestMethod]
        public void PagoCompletoCrédito()
        {
            //Configuracion
            Pago pago = new Pago();
            double monto = 0;
            foreach (Cuota c in _cliente.ObtenerCuotasImpagas())
            {
               monto = pago.agregarCuota(c);
            }
            //Ejecucion
            pago.Pagar(monto);
            var obtenido = _cliente.ObtenerCuotasImpagas().Count;
            //Validacion
            Assert.AreEqual(0, obtenido);
        }
     
    }
}
