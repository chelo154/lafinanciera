﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class LineaPago
    {
        public  int ID { get; set; }
        public virtual Cuota Cuota { get; set; }
        public virtual Pago Pago { get; set; }
        public double ImportePagado { get; set; }
        public double ImportePorCuota { get; set; }

        /*para el ORM de Entity*/
        public virtual  int NumeroCuota { get; set; }
        public virtual int NumeroPago { get; set; }
        /**/
        public LineaPago()
        {

        }
        public LineaPago(Cuota c)
        {
            Cuota = c;
        }
        public double calcularSubTotal()
        {
            double monto;
            var diasVencido = DateTime.Now.Subtract(Cuota.Vencimiento.Date);
            if (diasVencido.Days <= 0) monto = Cuota.Monto;
            else monto = Cuota.Monto + (Cuota.Monto * (Financiera.LaFinanciera.Reglas.PorcentajeDiarioInteres * diasVencido.Days) / 100); 
            return monto;
        }
        public void PagoCompletoCuota()
        {
            ImportePagado = calcularSubTotal();
            ImportePorCuota = Cuota.Monto;
            Cuota.PagoCompleto(this);
        }
        public double PagoParcialCuota(double monto)
        {
            double vuelto;
            if (monto >= Cuota.calcularSaldo())
            {
                
                Cuota.PagoCompleto(this);
                ImportePagado = calcularSubTotal();
                vuelto = monto - calcularSubTotal();
                return monto;
            }
            else
            {
                ImportePagado = monto;
                ImportePorCuota = monto;
                Cuota.PagoParcial(this);
                return 0;
            }
        }
    }
}
