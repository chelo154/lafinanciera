﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public sealed  class GrupoFinanciera
    {
      
         const string _groupID = "3f272b24-9134-4ebc-a700-bf9c26cc7fc3";
        private readonly static GrupoFinanciera _grupo = new GrupoFinanciera();
        //Hola Mundo xDXDXDXDXD
        private GrupoFinanciera()
        {

        }
        

        public static GrupoFinanciera Grupo => _grupo;
        
        public string GroupID { get => _groupID;  }
    }
}
