﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Cuota
    {
        public int NumeroCuota { get; set; }
        public Credito Credito { get; set; }
        public double Monto  { get; set; }
        public DateTime Vencimiento{ get; set; }
       
        public EstadoCuota Estado { get; set; }
        public virtual ICollection<LineaPago> LineasPago { get; set; }
        public Cuota()
        {
            LineasPago = new List<LineaPago>();
            this.Estado = EstadoCuota.NoPagada;
        }
        public Cuota(Credito c)
        {
            LineasPago = new List<LineaPago>();
            this.Estado = EstadoCuota.NoPagada;
            Credito = c;
        }
        internal void PagoCuotaAdelantada() {
			Monto = 0;
			Estado = EstadoCuota.Pagada;
		}
        internal void PagoCompleto(LineaPago linea)
        {
            
                Estado = EstadoCuota.Pagada;
                LineasPago.Add(linea);
                Credito.VerificarPagoCompleto();
            
        }
        internal double calcularSaldo()
        {
            double abonado,saldo;
            abonado = saldo = 0;
            foreach (LineaPago linea in LineasPago) abonado += linea.ImportePorCuota;
            saldo = Monto - abonado;
            return saldo;
        }

        internal void PagoParcial(LineaPago lineaPago)
        {
            Estado = EstadoCuota.PagadaParcialmente;
            LineasPago.Add(lineaPago);
        }
    }
}
