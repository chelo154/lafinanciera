﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Sesion
    {
        private string _usuario;
        private string _contrasena;
        public string Usuario{ get => _usuario; set => _usuario = value; }
        public string Contrasena { get => _contrasena; set => _contrasena = value; }
        public virtual Empleado Empleado { get; set; }
        public virtual int Legajo { get; set; }
        public Sesion()
        {
                
        }
    }
}
