﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Empleado
    {
        public int Legajo { get; set; }
        public string Apellido { get; set; }
        public string Nombrë { get; set; }
        public virtual Sesion Sesion { get; set; }
        public virtual ICollection<Pago> Pagos { get; set; }
        public virtual ICollection<Credito> Creditos { get; set; }
        public Empleado()
        {

        }



    }
}
