﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Credito
    {
        /*Propiedades*/
        
        public double MontoOriginal { get; set; }
        public int NumeroCredito { get; set; }
        public DateTime Fecha { get; set; }
        public EstadoCredito Estado { get; set; }
        public Plan Plan { get; set; }
        public virtual ICollection<Cuota> Cuotas { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual Empleado Empleado { get; set; }


        public Financiera financiera = Financiera.LaFinanciera;

        /*Para el ORM DE Entity Framwork*/
        public Int64 Dni { get; set; }
        public int Legajo { get; set; }

        

        public Credito() 
        {
            Cuotas = new List<Cuota>();
            Estado = EstadoCredito.Activo;
        }
		/*Metodos*/
		public double seleccionarModoPago(double monto, Plan plan){
            MontoOriginal = monto;
            double montoTotal=0;
			Plan = plan;
			GenerarCuotas();
			montoTotal = CalcularMontoTotal();
			return montoTotal;
		}
       
		private double CalcularMontoTotal()
		{
			double montoCuota, montoTotal;
			montoCuota = montoTotal = 0;
			montoCuota = CalcularMontoCuota();
			foreach (Cuota c in Cuotas) c.Monto = montoCuota;
			Descontar(Plan.Tipo);
			montoTotal = MontoOriginal +(MontoOriginal*
						Financiera.LaFinanciera.Reglas.PorcentajeInteresMensual)/100;
			return montoTotal;
		}

		private void Descontar(TipoPlan tipo)
		{
			if (tipo == TipoPlan.CuotaAdelantada)
			{

				var cuota = Cuotas.First();
				MontoOriginal -= cuota.Monto;
				cuota.PagoCuotaAdelantada();
			}
			else if (tipo == TipoPlan.CuotaVencida) {
				MontoOriginal -= MontoOriginal * Plan.Gastos/100;
			}
		}

		private double CalcularMontoCuota()
		{
			double montoCuota = MontoOriginal / (Financiera.LaFinanciera.Reglas.PorcentajeInteresMensual / Plan.CantidadCuotas); 
			return montoCuota;
		}

		public List<Cuota> ObtenerCuotasImpagas()
        {
            List<Cuota> cuotasImpagas = new List<Cuota>();
            cuotasImpagas.AddRange(Cuotas.Where<Cuota>(c => c.Estado == EstadoCuota.NoPagada || c.Estado == EstadoCuota.PagadaParcialmente
            || c.Estado == EstadoCuota.Vencida));
            return cuotasImpagas;
        }
        internal void VerificarPagoCompleto()
        {
            bool bandera = true;
            foreach (Cuota c in Cuotas)
            {
                if (c.Estado != EstadoCuota.Pagada) bandera = false;
                
            }
            if (bandera)
            {
                Estado = EstadoCredito.Finalizado;
            }
        }
        internal void GenerarCuotas()
        {
            var anio = DateTime.Now.Year;
            var mes = DateTime.Now.Month + 1;
            var dia = 10;
            if (mes > 12)
            {
                mes = 1;
                anio++;
            }
            
            for (int i=0; i< Plan.CantidadCuotas; i++)
            {
                Cuota c = new Cuota(this);
				c.Vencimiento = new DateTime(anio, mes, dia);
				Cuotas.Add(c);
                mes++;
                if (mes > 12)
                {
                    mes = 1;
                    anio++;
                }

            }
        }

    }
}
