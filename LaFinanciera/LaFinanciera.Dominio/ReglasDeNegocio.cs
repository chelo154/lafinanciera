﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class ReglasDeNegocio
    {
        private static ReglasDeNegocio _reglas = new ReglasDeNegocio();
		private double _montoMaximo = 100000;
        public double MontoMaximo { get;  set ; }
        public double PorcentajeDiarioInteres { get; set; }

		public double PorcentajeInteresMensual { get; set; }

		private const int _cantidadCreditosActivos = 3;/*No definieron que esto puede variar*/
        public static ReglasDeNegocio Reglas { get => _reglas; }

	

        /*Metodos para validar las Reglas de Negocio*/
        public bool validarCantidadCreditosActivosYMorosos(int cantidadCreditos) => cantidadCreditos <= _cantidadCreditosActivos;
        public bool validarMontoMaximo(int montoAOtorgar) => MontoMaximo <= montoAOtorgar;

        



    }
}
