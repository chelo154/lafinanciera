﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Financiera
    {
        /*La Instancio como Singleton, dado que solo tenemos que tener el conocimiento de una financiera*/
        private static readonly Financiera _financiera = new Financiera();
        private string _nombreComercial = "La Financiera";
        private string _razonSocial = "La Financiera SA";
        private long _cuit = 1234567892;
        public string NombreComercial { get => _nombreComercial; }
        public string RazonSocial{ get => _razonSocial; }
        public long Cuit { get => _cuit; }

        public string Domicilio { get; set; }
        public static Financiera LaFinanciera => _financiera;
        public ReglasDeNegocio Reglas { get =>ReglasDeNegocio.Reglas; }
        public ICollection<Credito> Creditos { get; set; }
        public Financiera()
        {
            
        }
       


    }
}
