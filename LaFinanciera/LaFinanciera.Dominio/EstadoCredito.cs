﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public enum EstadoCredito
    {
        Pendiente,
        Activo,
        Finalizado,
        PendienteFinalización,
        Moroso
    }
}
