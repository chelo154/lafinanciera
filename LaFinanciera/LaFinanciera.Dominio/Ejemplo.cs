﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Ejemplo
    {

        //Atributos
        public int NumeroUno { get; set; }
        public int NumeroDos { get; set; }
        public virtual Sesion Sesion { get; set; }
        //Métodos de la clase
        public int Suma() {

            return NumeroDos + NumeroUno;
                
        }


    }
}
