﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Pago
    {
        public virtual ICollection<LineaPago> LineasPago { get; set; }
        public DateTime Fecha { get; set; }
        public double Monto { get; set; }
        public int NumeroPago { get; set; }
        public virtual Empleado Empleado { get; set; }

        /*Para el ORM de Entity Framework*/
        public int Legajo { get; set; }
        public Pago()
        {
            LineasPago = new List<LineaPago>();
        }
        public void calcularTotal()
        {
            Monto = 0;
            foreach (LineaPago linea in LineasPago)
            {
                Monto += linea.calcularSubTotal();
            }
        }
        public void Pagar(double montoPagado)
        {
            if(montoPagado >= Monto)
            {
                foreach (LineaPago linea in LineasPago) linea.PagoCompletoCuota();
            }
            else
            {
                double saldo = montoPagado;
                foreach(LineaPago linea in LineasPago.OrderBy(c => c.Cuota.Vencimiento))
                {
                    saldo = linea.PagoParcialCuota(saldo);
                    if (saldo <= 0) break;
                }
            }
        }
        public double agregarCuota(Cuota c)
        {
            LineasPago.Add(new LineaPago(c));
            calcularTotal();
            return Monto;
        }
    }
}
