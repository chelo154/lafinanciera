﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Plan
    {
        public int NumeroPlan { get; set; }
        
        public int CantidadCuotas { get; set; }
        public double PorcentajeMensual { get; set; }
		public TipoPlan Tipo { get; set; }
        public double Gastos { get; set; }

        public Plan(int numeroPlan, TipoPlan tipo, int cantidadCuotas, double porcentajeMensual,double gastos)
        {
            this.NumeroPlan = numeroPlan;
			this.Tipo = tipo;
            this.CantidadCuotas = cantidadCuotas;
            this.PorcentajeMensual = porcentajeMensual;
            this.Gastos = gastos;
        }




        public Plan()
        {
        }

        public virtual double calcularGastosAdministrativos(double montoOriginal) => 0;

    }
}
