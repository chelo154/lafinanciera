﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Dominio
{
    public class Cliente
    {
        public Int64 Dni { get; set; }
        public string Apellido { get; set; }
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public Int64 Telefono { get; set; }
        public Int64 Sueldo { get; set; }
        public virtual ICollection<Credito> Creditos { get; set; }

        public Cliente(Int64 Dni, string Apellido, string Nombre, string Domicilio, Int64 Telefono, Int64 Sueldo) {

            this.Dni = Dni;
            this.Apellido = Apellido;
            this.Nombre = Nombre;
            this.Domicilio = Domicilio;
            this.Telefono = Telefono;
            this.Sueldo = Sueldo;
            Creditos = new List<Credito>();

        }

        public Cliente()
        {
            Creditos = new List<Credito>();
        }

        public List<Cuota> ObtenerCuotasImpagas()
        {
            List<Cuota> cuotasImpagas = new List<Cuota>();
            foreach (Credito credito in ObtenerCreditosActivosyMorosos())
            {
                cuotasImpagas.AddRange(credito.ObtenerCuotasImpagas());
            }
            return cuotasImpagas;
        }
        private List<Credito> ObtenerCreditosActivosyMorosos()
        {
            List<Credito> creditosActivosYMorosos = new List<Credito>();
            creditosActivosYMorosos.AddRange(Creditos.Where(c => c.Estado == EstadoCredito.Activo || c.Estado == EstadoCredito.Moroso));
            return creditosActivosYMorosos;
        }
        
    }
}
