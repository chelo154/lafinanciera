﻿using LaFinanciera.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaFinanciera.Datos;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion.Vista.Presentadores
{
    public class PresentadorSolicitarCredito
    {
        private InterfaceSolicitarCredito _InterfaceSolicitarCredito;
        private AdaptadorDatos Adaptador = new AdaptadorDatos();

        public PresentadorSolicitarCredito(InterfaceSolicitarCredito Vista)
        {

            _InterfaceSolicitarCredito = Vista;
            

        }

        public Cliente CaptarDatosCliente(Int64 dni) {

            
            Cliente cliente = RepositorioLocal.BuscarCliente(dni);
            if (cliente == null) MessageBox.Show("NO SE ENCONTRÓ CLIENTE");
            return cliente;


        }
        public List<Plan> CaptarPlanes() {

           // List<Plan> lista = RepositorioLocal.BuscarPlanes();
            
           return null;

        }

        public void Calcular(Plan plan, ref double monto, ref double montoCuota, ref double montoTotal, ref double intereses,ref double gastos, ref double importe) {

            montoCuota = (monto + (monto * plan.PorcentajeMensual * plan.CantidadCuotas) / 100)/plan.CantidadCuotas;
            montoTotal = monto + (monto * plan.PorcentajeMensual * plan.CantidadCuotas) / 100;
            intereses = montoTotal - monto;
            gastos = (plan.Gastos * monto)/100;
            importe = monto - gastos;
        }

        public Credito GenerarCredito() {

            Credito credito = new Credito();



            return credito;

        }


    }
}
