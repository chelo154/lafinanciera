﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaFinanciera.Dominio;

namespace LaFinanciera.Presentacion
{
    public class PresentadorInicio
    {
        //Crear Objeto del Modelo
        Ejemplo _Ejemplo = new Ejemplo();

        //Instanciar la Interface
        private InterfaceEjemplo _InterfaceEjemplo;

        public PresentadorInicio()
        {
        }

        //Crear constructor
        public PresentadorInicio(InterfaceEjemplo Vista) {

            _InterfaceEjemplo = Vista;

        }

        //Crear Metodos de conexión entre la vista y el modelo

        public void InicioSuma() {

            _Ejemplo.NumeroUno = Convert.ToInt32(_InterfaceEjemplo.TextoNumeroUno);
            _Ejemplo.NumeroDos = Convert.ToInt32(_InterfaceEjemplo.TextoNumeroDos);
            _InterfaceEjemplo.TextoResultado = _Ejemplo.Suma().ToString();


        }
        


    }
}
