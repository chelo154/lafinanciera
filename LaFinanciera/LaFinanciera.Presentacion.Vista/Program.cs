﻿using LaFinanciera.Datos;
using LaFinanciera.Dominio;
using LaFinanciera.Presentacion.Vista.Vistas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion
{
    static class Program
    {

       

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new VistaInicio());
        }
    }
}
