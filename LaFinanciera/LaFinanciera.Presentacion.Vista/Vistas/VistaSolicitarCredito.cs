﻿using LaFinanciera.Datos.ConectorAPI;
using LaFinanciera.Dominio;
using LaFinanciera.Presentacion.Vista.Presentadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion.Vista.Vistas
{
    public partial class VistaSolicitarCredito : Form, InterfaceSolicitarCredito
    {


        public PresentadorSolicitarCredito Presentador;
        public Cliente cliente;

        public VistaSolicitarCredito()
        {



            InitializeComponent();
            Presentador = new PresentadorSolicitarCredito(this);
            List<Plan> lista = Presentador.CaptarPlanes();
            List<String> modalidades = new List<String>();
            foreach (Plan p in lista) {

               // modalidades.Add(p.DescripcionPlan.ToString());
                
            }

            comboBox1.DataSource = modalidades;
            
            bindingSource1.DataSource = Presentador.GenerarCredito();

        }

        string InterfaceSolicitarCredito.TextoDniCliente { get => TextoDniCliente.Text; set => TextoDniCliente.Text = value; }
        string InterfaceSolicitarCredito.TextoNombre { get => TextoNombre.Text; set => TextoNombre.Text = value; }
        string InterfaceSolicitarCredito.TextoDomicilio { get => TextoDomicilio.Text; set => TextoDomicilio.Text = value; }
        string InterfaceSolicitarCredito.TextoTelefono { get => TextoTelefono.Text; set => TextoTelefono.Text = value; }
        string InterfaceSolicitarCredito.TextoSueldo { get => TextoSueldo.Text; set => TextoSueldo.Text = value; }
        string InterfaceSolicitarCredito.TextoMonto { get => TextoMonto.Text; set => TextoMonto.Text = value; }
        string InterfaceSolicitarCredito.TextoMontoCuota { get => TextoMontoCuota.Text; set => TextoMontoCuota.Text = value; }
        string InterfaceSolicitarCredito.TextoIntereses { get => TextoIntereses.Text; set => TextoIntereses.Text = value; }
        string InterfaceSolicitarCredito.TextoApellido { get => label.Text; set => label.Text = value; }
        string InterfaceSolicitarCredito.TextoGastos { get => TextoGastos.Text; set => TextoGastos.Text = value; }
        string InterfaceSolicitarCredito.TextoImporte { get => TextoImporte.Text; set => TextoImporte.Text = value; }

        private void VistaSolicitarCredito_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int creditos=0;                      
            cliente = Presentador.CaptarDatosCliente(Convert.ToInt64(TextoDniCliente.Text));

            if (cliente != null)
            {
                TextoApellido.Text = cliente.Apellido;
                TextoNombre.Text = cliente.Nombre;
                TextoDomicilio.Text = cliente.Domicilio;
                TextoTelefono.Text = Convert.ToString(cliente.Telefono);
                TextoSueldo.Text = Convert.ToString(cliente.Sueldo);
            }

            IConectorServicio AdaptadorAPI = AdministradorConexiones.Administrador.ObtenerConectorServicioPublico();
            AdaptadorAPI.Conectar();
            AdaptadorAPI.ValidarEstadoCliente(GrupoFinanciera.Grupo.GroupID, Convert.ToInt64(TextoDniCliente.Text), ref creditos);
            TextoCreditos.Text = Convert.ToString(creditos);

            if (creditos >= 3) {

                MessageBox.Show("EL CLIENTE POSEE MÁS DE 3 CREDITOS ACTIVOS");
                this.Dispose();

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            VistaGestionarCliente _VistaGestionarCliente = new VistaGestionarCliente();
            _VistaGestionarCliente.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double monto = Convert.ToDouble(TextoMonto.Text);
            double montoCuota=0;
            double montoTotal=0; 
            double intereses=0;
            double gastos = 0;
            double importe = 0;
            List<Plan> planes = Presentador.CaptarPlanes();
            Plan plan = new Plan();

            foreach (Plan p in planes) {

                if (comboBox1.SelectedIndex + 1 == p.NumeroPlan) plan = p;

            }


            Presentador.Calcular(plan,ref monto,ref montoCuota,ref montoTotal,ref intereses,ref gastos,ref importe);

            TextoMontoCuota.Text = Convert.ToString(montoCuota);
            TextoMontoTotal.Text = Convert.ToString(montoTotal);
            TextoIntereses.Text = Convert.ToString(intereses);
            TextoGastos.Text = Convert.ToString(gastos);
            TextoImporte.Text = Convert.ToString(importe);
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Credito credito = bindingSource1.Current as Credito;
           

        }

        
    }
}
