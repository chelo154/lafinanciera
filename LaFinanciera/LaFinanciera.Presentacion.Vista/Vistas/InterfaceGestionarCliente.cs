﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Presentacion
{
    public interface InterfaceGestionarCliente
    {
        String TextoDniCliente { get; set; }
        String TextoNombre { get; set; }
        String TextoDomicilio { get; set; }
        String TextoTelefono { get; set; }
        String TextoSueldo { get; set; }
        String TextoApellido { get; set; }


    }
}
