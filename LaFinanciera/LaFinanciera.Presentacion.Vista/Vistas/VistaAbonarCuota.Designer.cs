﻿namespace LaFinanciera.Presentacion.Vista.Vistas
{
    partial class VistaAbonarCuota
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.TextoDniCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.TextoNombre = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextoMontoTotal = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TextoMontoAPagar = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(342, 42);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Buscar Cuotas Impagas";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // TextoDniCliente
            // 
            this.TextoDniCliente.Location = new System.Drawing.Point(109, 44);
            this.TextoDniCliente.Name = "TextoDniCliente";
            this.TextoDniCliente.Size = new System.Drawing.Size(185, 20);
            this.TextoDniCliente.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "DNI Cliente:";
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(59, 120);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(372, 149);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // TextoNombre
            // 
            this.TextoNombre.Location = new System.Drawing.Point(109, 71);
            this.TextoNombre.Name = "TextoNombre";
            this.TextoNombre.ReadOnly = true;
            this.TextoNombre.Size = new System.Drawing.Size(250, 20);
            this.TextoNombre.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Nombre:";
            // 
            // TextoMontoTotal
            // 
            this.TextoMontoTotal.Location = new System.Drawing.Point(342, 286);
            this.TextoMontoTotal.Name = "TextoMontoTotal";
            this.TextoMontoTotal.ReadOnly = true;
            this.TextoMontoTotal.Size = new System.Drawing.Size(89, 20);
            this.TextoMontoTotal.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 289);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Monto Total:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(304, 323);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Confirmar Pago";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 289);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "Monto a Pagar:";
            // 
            // TextoMontoAPagar
            // 
            this.TextoMontoAPagar.Location = new System.Drawing.Point(142, 286);
            this.TextoMontoAPagar.Name = "TextoMontoAPagar";
            this.TextoMontoAPagar.Size = new System.Drawing.Size(80, 20);
            this.TextoMontoAPagar.TabIndex = 21;
            // 
            // VistaAbonarCuota
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 376);
            this.Controls.Add(this.TextoMontoAPagar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TextoMontoTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextoNombre);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextoDniCliente);
            this.Controls.Add(this.label1);
            this.Name = "VistaAbonarCuota";
            this.Text = "Abonar Cuota";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TextoDniCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.TextBox TextoNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextoMontoTotal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextoMontoAPagar;
    }
}