﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Presentacion
{
    public interface InterfaceEjemplo
    {
        //Entrada y salida de datos de la vista

        String TextoNumeroUno { get; set; }
        String TextoNumeroDos { get; set; }
        String TextoResultado { get; set; }



    }
}
