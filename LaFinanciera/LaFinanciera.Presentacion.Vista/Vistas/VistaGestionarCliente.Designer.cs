﻿namespace LaFinanciera.Presentacion.Vista.Vistas
{
    partial class VistaGestionarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextoApellido = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.TextoSueldo = new System.Windows.Forms.TextBox();
            this.TextoTelefono = new System.Windows.Forms.TextBox();
            this.TextoDomicilio = new System.Windows.Forms.TextBox();
            this.TextoNombre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TextoDniCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextoApellido
            // 
            this.TextoApellido.Location = new System.Drawing.Point(113, 109);
            this.TextoApellido.Name = "TextoApellido";
            this.TextoApellido.Size = new System.Drawing.Size(267, 20);
            this.TextoApellido.TabIndex = 40;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(60, 112);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(47, 13);
            this.label.TabIndex = 39;
            this.label.Text = "Apellido:";
            // 
            // TextoSueldo
            // 
            this.TextoSueldo.Location = new System.Drawing.Point(113, 215);
            this.TextoSueldo.Name = "TextoSueldo";
            this.TextoSueldo.Size = new System.Drawing.Size(101, 20);
            this.TextoSueldo.TabIndex = 38;
            this.TextoSueldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TextoTelefono
            // 
            this.TextoTelefono.Location = new System.Drawing.Point(113, 182);
            this.TextoTelefono.Name = "TextoTelefono";
            this.TextoTelefono.Size = new System.Drawing.Size(101, 20);
            this.TextoTelefono.TabIndex = 37;
            this.TextoTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TextoDomicilio
            // 
            this.TextoDomicilio.Location = new System.Drawing.Point(113, 146);
            this.TextoDomicilio.Name = "TextoDomicilio";
            this.TextoDomicilio.Size = new System.Drawing.Size(267, 20);
            this.TextoDomicilio.TabIndex = 36;
            // 
            // TextoNombre
            // 
            this.TextoNombre.Location = new System.Drawing.Point(113, 74);
            this.TextoNombre.Name = "TextoNombre";
            this.TextoNombre.Size = new System.Drawing.Size(267, 20);
            this.TextoNombre.TabIndex = 35;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(64, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Sueldo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(55, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Teléfono:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 149);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Domicilio:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Nombre:";
            // 
            // TextoDniCliente
            // 
            this.TextoDniCliente.Location = new System.Drawing.Point(113, 38);
            this.TextoDniCliente.Name = "TextoDniCliente";
            this.TextoDniCliente.Size = new System.Drawing.Size(185, 20);
            this.TextoDniCliente.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "DNI Cliente:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(300, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 43);
            this.button1.TabIndex = 41;
            this.button1.Text = "Agregar Cliente";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // VistaGestionarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 267);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextoApellido);
            this.Controls.Add(this.label);
            this.Controls.Add(this.TextoSueldo);
            this.Controls.Add(this.TextoTelefono);
            this.Controls.Add(this.TextoDomicilio);
            this.Controls.Add(this.TextoNombre);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextoDniCliente);
            this.Controls.Add(this.label1);
            this.Name = "VistaGestionarCliente";
            this.Text = "VistaGestionarCliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextoApellido;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox TextoSueldo;
        private System.Windows.Forms.TextBox TextoTelefono;
        private System.Windows.Forms.TextBox TextoDomicilio;
        private System.Windows.Forms.TextBox TextoNombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextoDniCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}