﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion.Vista.Vistas
{
    public partial class VistaAbonarCuota : Form, InterfaceAbonarCuota
    {
        public VistaAbonarCuota()
        {
            InitializeComponent();
            listView1.View = View.Details;
            listView1.Columns.Add("Credito",70);
            listView1.Columns.Add("Cuota", 70);
            listView1.Columns.Add("Vencimiento",90);
            listView1.Columns.Add("Saldo", 70);
            listView1.Columns.Add("Monto", 70);
           

        }

        string InterfaceAbonarCuota.TextoDniCliente { get => TextoDniCliente.Text; set => TextoDniCliente.Text = value; }
        string InterfaceAbonarCuota.TextoNombre { get => TextoNombre.Text; set => TextoNombre.Text = value; }
        string InterfaceAbonarCuota.TextoMontoAPagar { get => TextoMontoAPagar.Text; set => TextoMontoAPagar.Text = value; }
        string InterfaceAbonarCuota.TextoMontoTotal { get => TextoMontoTotal.Text; set => TextoMontoTotal.Text = value; }
    }
}
