﻿using LaFinanciera.Presentacion.Vista.Presentadores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion.Vista.Vistas
{
    public partial class VistaGestionarCliente : Form, InterfaceGestionarCliente
    {


        public PresentadorGestionarCliente Presentador;


        public VistaGestionarCliente()
        {
            InitializeComponent();
            Presentador = new PresentadorGestionarCliente(this);
        }


        string InterfaceGestionarCliente.TextoDniCliente { get => TextoDniCliente.Text; set => TextoDniCliente.Text = value; }
        string InterfaceGestionarCliente.TextoNombre { get => TextoNombre.Text; set => TextoNombre.Text = value; }
        string InterfaceGestionarCliente.TextoDomicilio { get => TextoDomicilio.Text; set => TextoDomicilio.Text = value; }
        string InterfaceGestionarCliente.TextoTelefono { get => TextoTelefono.Text; set => TextoTelefono.Text = value; }
        string InterfaceGestionarCliente.TextoSueldo { get => TextoSueldo.Text; set => TextoSueldo.Text = value; }
        string InterfaceGestionarCliente.TextoApellido { get => TextoApellido.Text; set => TextoApellido.Text = value; }

        private void button1_Click(object sender, EventArgs e)
        {

            Presentador.AgregarCliente(Convert.ToInt64(TextoDniCliente.Text), TextoApellido.Text, TextoNombre.Text, TextoDomicilio.Text, Convert.ToInt64(TextoTelefono.Text),Convert.ToInt64(TextoSueldo.Text));
            this.Dispose();

        }
    }
}
