﻿using LaFinanciera.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LaFinanciera.Presentacion.Vista.Vistas
{
    public partial class VistaInicio : Form, InterfaceInicio
    {

        public PresentadorInicio Presentador;


        public VistaInicio()
        {
            InitializeComponent();
            Presentador = new PresentadorInicio();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VistaSolicitarCredito _VistaSolicitarCredito = new VistaSolicitarCredito();
            _VistaSolicitarCredito.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VistaAbonarCuota _VistaAbonarCuota = new VistaAbonarCuota();
            _VistaAbonarCuota.Show();
        }
    }
}
