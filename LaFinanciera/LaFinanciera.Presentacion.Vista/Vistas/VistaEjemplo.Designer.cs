﻿namespace LaFinanciera.Presentacion.Vista
{
    partial class VistaEjemplo
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextoNumeroUno = new System.Windows.Forms.TextBox();
            this.TextoNumeroDos = new System.Windows.Forms.TextBox();
            this.TextoResultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(107, 124);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Suma";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "NumeroUno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "NumeroDos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Resultado";
            // 
            // TextoNumeroUno
            // 
            this.TextoNumeroUno.Location = new System.Drawing.Point(107, 24);
            this.TextoNumeroUno.Name = "TextoNumeroUno";
            this.TextoNumeroUno.Size = new System.Drawing.Size(100, 20);
            this.TextoNumeroUno.TabIndex = 4;
            // 
            // TextoNumeroDos
            // 
            this.TextoNumeroDos.Location = new System.Drawing.Point(107, 52);
            this.TextoNumeroDos.Name = "TextoNumeroDos";
            this.TextoNumeroDos.Size = new System.Drawing.Size(100, 20);
            this.TextoNumeroDos.TabIndex = 5;
            // 
            // TextoResultado
            // 
            this.TextoResultado.Location = new System.Drawing.Point(107, 78);
            this.TextoResultado.Name = "TextoResultado";
            this.TextoResultado.Size = new System.Drawing.Size(100, 20);
            this.TextoResultado.TabIndex = 6;
            // 
            // VistaInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 185);
            this.Controls.Add(this.TextoResultado);
            this.Controls.Add(this.TextoNumeroDos);
            this.Controls.Add(this.TextoNumeroUno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "VistaInicio";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Inicio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextoNumeroUno;
        private System.Windows.Forms.TextBox TextoNumeroDos;
        private System.Windows.Forms.TextBox TextoResultado;
    }
}

