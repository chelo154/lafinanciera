﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Presentacion
{
    public interface InterfaceAbonarCuota
    {

        String TextoDniCliente { get; set; }
        String TextoNombre { get; set; }
        String TextoMontoAPagar { get; set; }
        String TextoMontoTotal { get; set; }



    }
}
