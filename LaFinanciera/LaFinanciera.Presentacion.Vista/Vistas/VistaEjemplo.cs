﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LaFinanciera.Presentacion;


namespace LaFinanciera.Presentacion.Vista
{
    public partial class VistaEjemplo : Form, InterfaceEjemplo
    {
        //Seteo de entradas y salidas de la interface

        string InterfaceEjemplo.TextoNumeroUno { get => TextoNumeroUno.Text; set => TextoNumeroUno.Text = value; }
        string InterfaceEjemplo.TextoNumeroDos { get => TextoNumeroDos.Text; set => TextoNumeroDos.Text = value; }
        string InterfaceEjemplo.TextoResultado { get => TextoResultado.Text; set => TextoResultado.Text = value; }
        PresentadorInicio Presentador;


        public VistaEjemplo()
        {
            InitializeComponent();
            //Creación del presentador
            Presentador = new PresentadorInicio(this);
           
        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        //Eventos de la vista

        private void button1_Click(object sender, EventArgs e)
        {
            //Uso del método del modelo
            Presentador.InicioSuma();

        }

       
    }
}
