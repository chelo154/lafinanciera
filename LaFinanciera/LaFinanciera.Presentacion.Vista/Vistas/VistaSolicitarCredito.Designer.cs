﻿namespace LaFinanciera.Presentacion.Vista.Vistas
{
    partial class VistaSolicitarCredito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.TextoDniCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextoMonto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TextoNombre = new System.Windows.Forms.TextBox();
            this.TextoDomicilio = new System.Windows.Forms.TextBox();
            this.TextoTelefono = new System.Windows.Forms.TextBox();
            this.TextoSueldo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TextoMontoCuota = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TextoMontoTotal = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TextoIntereses = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.TextoApellido = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.TextoCreditos = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.TextoGastos = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.TextoImporte = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(319, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Verificar Estado";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // TextoDniCliente
            // 
            this.TextoDniCliente.Location = new System.Drawing.Point(104, 31);
            this.TextoDniCliente.Name = "TextoDniCliente";
            this.TextoDniCliente.Size = new System.Drawing.Size(185, 20);
            this.TextoDniCliente.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "DNI Cliente:";
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.bindingSource1, "Plan", true));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(231, 301);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(215, 21);
            this.comboBox1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(118, 304);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Modalidad de Credito";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(185, 341);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Monto:";
            // 
            // TextoMonto
            // 
            this.TextoMonto.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bindingSource1, "MontoOriginal", true));
            this.TextoMonto.Location = new System.Drawing.Point(231, 338);
            this.TextoMonto.Name = "TextoMonto";
            this.TextoMonto.Size = new System.Drawing.Size(72, 20);
            this.TextoMonto.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nombre:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Domicilio:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Teléfono:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(55, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Sueldo:";
            // 
            // TextoNombre
            // 
            this.TextoNombre.Location = new System.Drawing.Point(104, 67);
            this.TextoNombre.Name = "TextoNombre";
            this.TextoNombre.ReadOnly = true;
            this.TextoNombre.Size = new System.Drawing.Size(267, 20);
            this.TextoNombre.TabIndex = 14;
            // 
            // TextoDomicilio
            // 
            this.TextoDomicilio.Location = new System.Drawing.Point(104, 139);
            this.TextoDomicilio.Name = "TextoDomicilio";
            this.TextoDomicilio.ReadOnly = true;
            this.TextoDomicilio.Size = new System.Drawing.Size(267, 20);
            this.TextoDomicilio.TabIndex = 15;
            // 
            // TextoTelefono
            // 
            this.TextoTelefono.Location = new System.Drawing.Point(104, 175);
            this.TextoTelefono.Name = "TextoTelefono";
            this.TextoTelefono.ReadOnly = true;
            this.TextoTelefono.Size = new System.Drawing.Size(101, 20);
            this.TextoTelefono.TabIndex = 16;
            this.TextoTelefono.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // TextoSueldo
            // 
            this.TextoSueldo.Location = new System.Drawing.Point(104, 208);
            this.TextoSueldo.Name = "TextoSueldo";
            this.TextoSueldo.ReadOnly = true;
            this.TextoSueldo.Size = new System.Drawing.Size(101, 20);
            this.TextoSueldo.TabIndex = 17;
            this.TextoSueldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Location = new System.Drawing.Point(12, 278);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(470, 2);
            this.label8.TabIndex = 18;
            // 
            // TextoMontoCuota
            // 
            this.TextoMontoCuota.Location = new System.Drawing.Point(153, 379);
            this.TextoMontoCuota.Name = "TextoMontoCuota";
            this.TextoMontoCuota.ReadOnly = true;
            this.TextoMontoCuota.Size = new System.Drawing.Size(72, 20);
            this.TextoMontoCuota.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(57, 382);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Monto Por Cuota:";
            // 
            // TextoMontoTotal
            // 
            this.TextoMontoTotal.Location = new System.Drawing.Point(153, 405);
            this.TextoMontoTotal.Name = "TextoMontoTotal";
            this.TextoMontoTotal.ReadOnly = true;
            this.TextoMontoTotal.Size = new System.Drawing.Size(72, 20);
            this.TextoMontoTotal.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(80, 408);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Monto Total:";
            // 
            // TextoIntereses
            // 
            this.TextoIntereses.Location = new System.Drawing.Point(153, 431);
            this.TextoIntereses.Name = "TextoIntereses";
            this.TextoIntereses.ReadOnly = true;
            this.TextoIntereses.Size = new System.Drawing.Size(72, 20);
            this.TextoIntereses.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(94, 434);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Intereses:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(306, 387);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(123, 54);
            this.button2.TabIndex = 25;
            this.button2.Text = "Confirmar Credito";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(306, 200);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(127, 35);
            this.button3.TabIndex = 26;
            this.button3.Text = "Gestionar Nuevo Cliente";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // TextoApellido
            // 
            this.TextoApellido.Location = new System.Drawing.Point(104, 102);
            this.TextoApellido.Name = "TextoApellido";
            this.TextoApellido.ReadOnly = true;
            this.TextoApellido.Size = new System.Drawing.Size(267, 20);
            this.TextoApellido.TabIndex = 28;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(51, 105);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(47, 13);
            this.label.TabIndex = 27;
            this.label.Text = "Apellido:";
            // 
            // TextoCreditos
            // 
            this.TextoCreditos.Location = new System.Drawing.Point(104, 243);
            this.TextoCreditos.Name = "TextoCreditos";
            this.TextoCreditos.ReadOnly = true;
            this.TextoCreditos.Size = new System.Drawing.Size(101, 20);
            this.TextoCreditos.TabIndex = 30;
            this.TextoCreditos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(50, 246);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Creditos:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(319, 336);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 31;
            this.button4.Text = "Calcular";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // TextoGastos
            // 
            this.TextoGastos.Location = new System.Drawing.Point(153, 457);
            this.TextoGastos.Name = "TextoGastos";
            this.TextoGastos.ReadOnly = true;
            this.TextoGastos.Size = new System.Drawing.Size(72, 20);
            this.TextoGastos.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(101, 460);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 32;
            this.label13.Text = "Gastos:";
            // 
            // TextoImporte
            // 
            this.TextoImporte.Location = new System.Drawing.Point(357, 457);
            this.TextoImporte.Name = "TextoImporte";
            this.TextoImporte.ReadOnly = true;
            this.TextoImporte.Size = new System.Drawing.Size(72, 20);
            this.TextoImporte.TabIndex = 35;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(305, 460);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "Importe:";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(LaFinanciera.Dominio.Credito);
            // 
            // VistaSolicitarCredito
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(494, 483);
            this.Controls.Add(this.TextoImporte);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.TextoGastos);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.TextoCreditos);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TextoApellido);
            this.Controls.Add(this.label);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.TextoIntereses);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TextoMontoTotal);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TextoMontoCuota);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TextoSueldo);
            this.Controls.Add(this.TextoTelefono);
            this.Controls.Add(this.TextoDomicilio);
            this.Controls.Add(this.TextoNombre);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextoMonto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TextoDniCliente);
            this.Controls.Add(this.label1);
            this.Name = "VistaSolicitarCredito";
            this.Text = "Solicitud de Crédito";
            this.Load += new System.EventHandler(this.VistaSolicitarCredito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox TextoDniCliente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextoMonto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TextoNombre;
        private System.Windows.Forms.TextBox TextoDomicilio;
        private System.Windows.Forms.TextBox TextoTelefono;
        private System.Windows.Forms.TextBox TextoSueldo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TextoMontoCuota;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TextoMontoTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TextoIntereses;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox TextoApellido;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox TextoCreditos;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox TextoGastos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TextoImporte;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}