﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaFinanciera.Dominio;

namespace LaFinanciera.Datos
{
	public static class RepositorioLocal
	{
		public static List<Sesion> Sesiones { get; set; }
		public static List<Plan> Planes { get; set; }
		public static List<Cliente> Clientes{ get; set; }
		public static List<Empleado> Empleados{ get; set; }

		public static void IniciarCliente() {

             Clientes = new List<Cliente>();
            Clientes.Add(new Cliente(33000001, "Nuñez", "Marcelo", "Maipu 1457", 4655, 4432));
            Clientes.Add(new Cliente(33000002, "Darpino", "Antonella", "Jujuy 23", 45665, 6556));
            Clientes.Add(new Cliente(33000003, "Taddei", "Ivan", "José Colombres 789", 1556, 15000));
            Clientes.Add(new Cliente(33000004, "Tarchini", "Franco", "Rivadavia 45", 516, 8597));
            Clientes.Add(new Cliente(33000100, "Frank", "Peter", "Allá 457", 12132, 2045));

		}
		public static Cliente BuscarCliente(Int64 dni)
		{
			Cliente cliente = new Cliente();

			foreach (Cliente c in Clientes)
			{
				if (c.Dni == dni)
				{
					cliente = c;
					break;
				}
				else cliente = null;
			}
			return cliente;
		}
        public static Cliente BuscarCliente(string nombre, string apellido)
        {
            var cliente = Clientes.Find(c => c.Nombre.Equals(nombre) && c.Apellido.Equals(apellido));
            return cliente;
        }



            public static void crearPlanes() {

            Planes = new List<Plan>();

            Planes.Add(new Plan(1, TipoPlan.CuotaAdelantada, 3, 10,0));
            Planes.Add(new Plan(2, TipoPlan.CuotaAdelantada, 6, 20,0));
            
            Planes.Add(new Plan(3, TipoPlan.CuotaVencida, 3, 12,2));
            Planes.Add(new Plan(4, TipoPlan.CuotaVencida, 6, 22,4));
		}
        public static Plan BuscarPlanes(int numeroPlan)
        {
            var plan = Planes.Find(c => c.NumeroPlan == numeroPlan);
            return plan;
        }
        public static Sesion obtenerSesion(string usuario)
        {
           return  Sesiones.Find(c=>c.Usuario == usuario);
        }
		public static Empleado BuscarEmpleado(int legajo) => Empleados.Find(c => c.Legajo == legajo);


        }

    }
    





