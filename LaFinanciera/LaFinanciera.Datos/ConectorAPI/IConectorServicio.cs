﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Datos.ConectorAPI
{
    public interface IConectorServicio
    {
         void Conectar();
        bool ValidarEstadoCliente(string codigoFinanciera, long dni, ref int cantidadCreditosActivos);
        bool InformarCreditoOtorgado(string codigoFinanciera, long dni, string identificadorCredito, double montoOtorgado);
        bool InformarCreditoFinalizado(string codigoFinanciera, long dni, string identificadorCredito);
    }
}
