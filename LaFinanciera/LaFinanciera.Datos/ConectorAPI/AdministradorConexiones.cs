﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Datos.ConectorAPI
{
    public sealed class AdministradorConexiones
    {
        private readonly static AdministradorConexiones _administrador = new AdministradorConexiones();

        public AdministradorConexiones()
        {

        }

        public static AdministradorConexiones Administrador { get => _administrador; }
        public IConectorServicio ObtenerConectorServicioPublico() => new ConectorServicioPublico();
      
    }
}
