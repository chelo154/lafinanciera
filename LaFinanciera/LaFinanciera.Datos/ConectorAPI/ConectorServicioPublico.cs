﻿using LaFinanciera.Datos.ServicioPublicoCredito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LaFinanciera.Dominio;

namespace LaFinanciera.Datos.ConectorAPI
{
    public class ConectorServicioPublico : IConectorServicio
    {
        public ResultadoEstadoCliente EstadoCliente { get; set; }
        public ServicioPublicoCreditoClient Cliente { get; set; }
        public ResultadoOperacion Resultado { get; set; }

        public ConectorServicioPublico()
        {

        }

        public void Conectar()
        {
            Cliente = new ServicioPublicoCreditoClient();
        }

        

        public bool InformarCreditoOtorgado(string codigoFinanciera, long dni, string identificadorCredito, double montoOtorgado )
        {
            
            try
            {
                Resultado = Cliente.InformarCreditoOtorgado(codigoFinanciera,Convert.ToInt32(dni), identificadorCredito, montoOtorgado);
                return Resultado.OperacionValida;
            }catch
            {
                return false;
            }
            
        }

        public bool InformarCreditoFinalizado(string codigoFinanciera, long dni, string identificadorCredito)
        {
            try
            {
                Resultado = Cliente.InformarCreditoFinalizado(codigoFinanciera, Convert.ToInt32(dni), identificadorCredito);
                return Resultado.OperacionValida;
            }
            catch
            {
                return false;
            }
        }

        public bool ValidarEstadoCliente(string codigoFinanciera, long dni, ref int cantidadCreditosActivos)
        {
            /*Valida que el Cliente no tenga deudas y devuelve por referencia la cantidad de creditos activos que posee*/
            try
            {
                EstadoCliente = Cliente.ObtenerEstadoCliente(codigoFinanciera, Convert.ToInt32(dni));
                if (EstadoCliente.ConsultaValida)
                {
                    if (EstadoCliente.TieneDeudas) return false;
                    else
                    {
                        cantidadCreditosActivos = EstadoCliente.CantidadCreditosActivos;
                        return true;
                    }
                }
                else
                {
                    throw new Exception(EstadoCliente.Error);
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
