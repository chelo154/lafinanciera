﻿using LaFinanciera.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Datos
{
    public class AdaptadorDatos
    {
        private LaFinancieraContext _contexto;
        public AdaptadorDatos()
        {
            _contexto = new LaFinancieraContext();
        }
        public Cliente buscarCliente(Int64 dni)
        {
            return _contexto.Clientes.Find(dni);
        }
        public void AgregarCliente(Cliente nuevo)
        {
            _contexto.Clientes.Add(nuevo);
            _contexto.SaveChanges();
        }
        public void EliminarCliente(Int64 dni)
        {
            _contexto.Clientes.Remove(_contexto.Clientes.Find(dni));
        }
        
    }
}
