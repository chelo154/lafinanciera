namespace LaFinanciera.Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Dni = c.Long(nullable: false, identity: true),
                        Apellido = c.String(),
                        Nombre = c.String(),
                        Domicilio = c.String(),
                        Telefono = c.Long(nullable: false),
                        Sueldo = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Dni);
            
            CreateTable(
                "dbo.Creditos",
                c => new
                    {
                        NumeroCredito = c.Int(nullable: false, identity: true),
                        MontoOriginal = c.Double(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        Estado = c.Int(nullable: false),
                        Dni = c.Long(nullable: false),
                        Legajo = c.Int(nullable: false),
                        Plan_NumeroPlan = c.Int(),
                    })
                .PrimaryKey(t => t.NumeroCredito)
                .ForeignKey("dbo.Clientes", t => t.Dni, cascadeDelete: true)
                .ForeignKey("dbo.Empleados", t => t.Legajo, cascadeDelete: true)
                .ForeignKey("dbo.Planes", t => t.Plan_NumeroPlan)
                .Index(t => t.Dni)
                .Index(t => t.Legajo)
                .Index(t => t.Plan_NumeroPlan);
            
            CreateTable(
                "dbo.Cuotas",
                c => new
                    {
                        NumeroCuota = c.Int(nullable: false, identity: true),
                        Monto = c.Double(nullable: false),
                        Vencimiento = c.DateTime(nullable: false),
                        Saldo = c.Double(nullable: false),
                        Estado = c.Int(nullable: false),
                        Credito_NumeroCredito = c.Int(),
                    })
                .PrimaryKey(t => t.NumeroCuota)
                .ForeignKey("dbo.Creditos", t => t.Credito_NumeroCredito)
                .Index(t => t.Credito_NumeroCredito);
            
            CreateTable(
                "dbo.LineasDePago",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NumeroCuota = c.Int(nullable: false),
                        NumeroPago = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cuotas", t => t.NumeroCuota, cascadeDelete: true)
                .ForeignKey("dbo.Pagos", t => t.NumeroPago, cascadeDelete: true)
                .Index(t => t.NumeroCuota)
                .Index(t => t.NumeroPago);
            
            CreateTable(
                "dbo.Pagos",
                c => new
                    {
                        NumeroPago = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Monto = c.Double(nullable: false),
                        Legajo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NumeroPago)
                .ForeignKey("dbo.Empleados", t => t.Legajo, cascadeDelete: true)
                .Index(t => t.Legajo);
            
            CreateTable(
                "dbo.Empleados",
                c => new
                    {
                        Legajo = c.Int(nullable: false, identity: true),
                        Apellido = c.String(),
                        Nombrë = c.String(),
                        Sesion_Usuario = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Legajo)
                .ForeignKey("dbo.Sesions", t => t.Sesion_Usuario)
                .Index(t => t.Sesion_Usuario);
            
            CreateTable(
                "dbo.Sesions",
                c => new
                    {
                        Usuario = c.String(nullable: false, maxLength: 128),
                        Contrasena = c.String(),
                        Legajo = c.Int(nullable: false),
                        Empleado_Legajo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Usuario)
                .ForeignKey("dbo.Empleados", t => t.Empleado_Legajo)
                .Index(t => t.Empleado_Legajo);
            
            CreateTable(
                "dbo.Planes",
                c => new
                    {
                        NumeroPlan = c.Int(nullable: false, identity: true),
                        DescripcionPlan = c.String(),
                        CantidadCuotas = c.Int(nullable: false),
                        PorcentajeMensual = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.NumeroPlan);
            
            CreateTable(
                "dbo.PlanesCuotaAdelantada",
                c => new
                    {
                        NumeroPlan = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.NumeroPlan)
                .ForeignKey("dbo.Planes", t => t.NumeroPlan)
                .Index(t => t.NumeroPlan);
            
            CreateTable(
                "dbo.PlanesCuotaVencida",
                c => new
                    {
                        NumeroPlan = c.Int(nullable: false),
                        Gastos = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.NumeroPlan)
                .ForeignKey("dbo.Planes", t => t.NumeroPlan)
                .Index(t => t.NumeroPlan);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PlanesCuotaVencida", "NumeroPlan", "dbo.Planes");
            DropForeignKey("dbo.PlanesCuotaAdelantada", "NumeroPlan", "dbo.Planes");
            DropForeignKey("dbo.Creditos", "Plan_NumeroPlan", "dbo.Planes");
            DropForeignKey("dbo.LineasDePago", "NumeroPago", "dbo.Pagos");
            DropForeignKey("dbo.Empleados", "Sesion_Usuario", "dbo.Sesions");
            DropForeignKey("dbo.Sesions", "Empleado_Legajo", "dbo.Empleados");
            DropForeignKey("dbo.Pagos", "Legajo", "dbo.Empleados");
            DropForeignKey("dbo.Creditos", "Legajo", "dbo.Empleados");
            DropForeignKey("dbo.LineasDePago", "NumeroCuota", "dbo.Cuotas");
            DropForeignKey("dbo.Cuotas", "Credito_NumeroCredito", "dbo.Creditos");
            DropForeignKey("dbo.Creditos", "Dni", "dbo.Clientes");
            DropIndex("dbo.PlanesCuotaVencida", new[] { "NumeroPlan" });
            DropIndex("dbo.PlanesCuotaAdelantada", new[] { "NumeroPlan" });
            DropIndex("dbo.Sesions", new[] { "Empleado_Legajo" });
            DropIndex("dbo.Empleados", new[] { "Sesion_Usuario" });
            DropIndex("dbo.Pagos", new[] { "Legajo" });
            DropIndex("dbo.LineasDePago", new[] { "NumeroPago" });
            DropIndex("dbo.LineasDePago", new[] { "NumeroCuota" });
            DropIndex("dbo.Cuotas", new[] { "Credito_NumeroCredito" });
            DropIndex("dbo.Creditos", new[] { "Plan_NumeroPlan" });
            DropIndex("dbo.Creditos", new[] { "Legajo" });
            DropIndex("dbo.Creditos", new[] { "Dni" });
            DropTable("dbo.PlanesCuotaVencida");
            DropTable("dbo.PlanesCuotaAdelantada");
            DropTable("dbo.Planes");
            DropTable("dbo.Sesions");
            DropTable("dbo.Empleados");
            DropTable("dbo.Pagos");
            DropTable("dbo.LineasDePago");
            DropTable("dbo.Cuotas");
            DropTable("dbo.Creditos");
            DropTable("dbo.Clientes");
        }
    }
}
