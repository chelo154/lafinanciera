﻿using LaFinanciera.Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Datos
{
    class LaFinancieraContext: DbContext
    {
        public LaFinancieraContext():base("La Financiera")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           /* modelBuilder.Entity<Cliente>().HasKey(c => c.Dni).ToTable("Clientes");
            modelBuilder.Entity<Credito>().HasKey(c => c.NumeroCredito).ToTable("Creditos");
            modelBuilder.Entity<Cuota>().HasKey(c => c.NumeroCuota).ToTable("Cuotas");
            modelBuilder.Entity<Empleado>().HasKey(c => c.Legajo).ToTable("Empleados");
            modelBuilder.Entity<Pago>().HasKey(c => c.NumeroPago).ToTable("Pagos");
            modelBuilder.Entity<LineaPago>().HasKey(c => c.ID).ToTable("LineasDePago");
            modelBuilder.Entity<Plan>().HasKey(c => c.NumeroPlan).ToTable("Planes");
            modelBuilder.Entity<PlanCuotaAdelantada>().ToTable("PlanesCuotaAdelantada");
            modelBuilder.Entity<PlanCuotaVencida>().ToTable("PlanesCuotaVencida");
            modelBuilder.Entity<Sesion>().HasKey(c => c.Usuario).HasRequired(c => c.Empleado).WithRequiredDependent();*/

        }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Credito> Creditos { get; set; }
        public DbSet<Cuota> Cuotas { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Pago> Pagos { get; set; }
        public DbSet<LineaPago> LineaPagos { get; set; }
        public DbSet<Plan> Planes { get; set; }
        //public DbSet<PlanCuotaAdelantada> PlanesCuotaAdelantada { get; set; }
        //public DbSet<PlanCuotaVencida> PlanesCuotaVencida { get; set; }
        public DbSet<Sesion> Sesiones { get; set; }
        
    }
}
