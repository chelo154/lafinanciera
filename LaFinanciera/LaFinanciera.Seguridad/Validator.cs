﻿using LaFinanciera.Datos;
using LaFinanciera.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaFinanciera.Seguridad
{
    class Validator
    {
        int _cantidadIntentos;
        public Validator()
        {
            _cantidadIntentos = 0;
        }
        public bool ValidarSesion(Sesion s)
        {
            if (_cantidadIntentos == 3) throw new OperationCanceledException("No puede iniciar Sesion"); 
            Sesion original = RepositorioLocal.obtenerSesion(s.Usuario);
            if (original.Contrasena.Equals(s.Contrasena)) return true;
            else
            {
                _cantidadIntentos++;
                return false;
            }
        }
    }
}
